BIJ1 font
=========

This is the BIJ1 font. This font is a fork of Mada, created by Khaled Hosny and
Paul D. Hunt. The intention of our changes is to make this font look friendlier
and rounder.

This font is available under the Open Font License (OFL).
